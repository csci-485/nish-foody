from re import template
from telnetlib import SE
from urllib import request
from django.contrib.auth import login
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.views import LoginView
from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.views.generic.base import TemplateView
from django.views.generic import View
from django.views.generic.edit import FormView, BaseUpdateView
from django.contrib.messages.views import SuccessMessageMixin
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.contrib import messages
from django.views.decorators.http import require_http_methods
from .models import Seller, Item, Category, Allergy, Order, OrderItem
from .forms import SellerForm, UserProfileForm, ItemForm, CartForm
from .utils import send_email


# Use this LoginRequiredMixin to restrict views, add it before, order matters
class CustomLoginView(LoginView):
    template_name = "webapp/login.html"
    fields = "__all__"
    redirect_authenticated_user = True


class RegisterPage(FormView):
    template_name = "webapp/register.html"
    form_class = UserCreationForm
    redirect_authenticated_user = True

    success_url = reverse_lazy("home")

    def form_valid(self, form):
        user = form.save()
        if user is not None:
            login(self.request, user)
        return super(RegisterPage, self).form_valid(form)

    def get(self, *args, **kwargs):
        if self.request.user.is_authenticated:
            return redirect("home")
        return super(RegisterPage, self).get(*args, **kwargs)

# search page, <created by: Gabriel, updated by: >
class SearchPage(TemplateView):
    template_name = "webapp/search.html"

    def get_queryset(self):         # search results
        qs = Item.objects.all()
        if 'category' in self.request.GET and len(self.request.GET['category']):
            qs = qs.filter(category=int(self.request.GET['category']))
        if 'seller' in self.request.GET and len(self.request.GET['seller']):
            qs = qs.filter(seller=int(self.request.GET['seller']))
        if 'allergy' in self.request.GET and len(self.request.GET['allergy']):
            qs = qs.exclude(allergy=int(self.request.GET['allergy']))
        if 'maxprice' in self.request.GET:
            qs = qs.filter(price__lte=float(self.request.GET['maxprice']))
        if 'avail' in self.request.GET:
            qs = qs.filter(availability=int(self.request.GET['avail']))
        if 'search' in self.request.GET:
            search_str = self.request.GET['search']
            search_str = search_str.replace('+',' ')
            qs = qs.filter(item_name__icontains=search_str)
        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'search' in self.request.GET:
            search_str = self.request.GET['search']
            if len(search_str):
                search_str = search_str.replace('+',' ')
                context['current_search'] = "You searched for " + search_str
        context['results_list'] = self.get_queryset()
        context['categories'] = Category.objects.all()
        context['allergies'] = Allergy.objects.all()
        context['sellers'] = Seller.objects.all()
        return context

# home page/landing page, <created by: Bhavik, updated by:Sneh >
class HomePage(TemplateView):
    template_name = "webapp/home.html"
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["sellers"] = Seller.objects.filter(approved=True)
        return context

# payment info page, <created by: sneh, updated by: sneh>
class PaymentInfoView(TemplateView):
    template_name = "webapp/payment.html"
    # success_url = reverse_lazy("profile", kwargs={'tabname': 'orders'})
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context
        # return redirect(reverse_lazy("profile", kwargs={'tabname': 'orders'}))


# seller page, <created by: ____, updated by: >
class SellerPage(TemplateView):
    template_name = "webapp/seller.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


# cart, <created by:Bhavik, updated by: >
class CartView(TemplateView):
    template_name = "webapp/menu.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


################################################################################
# order create, <created by: Bhavik, updated by: Dalon>
################################################################################
@method_decorator(login_required, name="dispatch")
class CartCheckoutView(SuccessMessageMixin, View):

    # submitting a form with some data about orders
    def post(self, request, *args, **kwargs):
        print(self.request.POST, type(self.request.POST.keys()))

        # get the user id from the request
        user_id = request.user.id

        # seller_id, item_id will be fetch from the data sent by the front-end
        seller_id = int(self.request.POST.get("input-cart-seller-id"))

        # fetch different types of items in the order
        variety = int(self.request.POST.get("input-cart-variety-count"))
        # if we have at-lease one item, we create a new order
        if variety:
            order = Order()
            order.seller = Seller.objects.get(id=seller_id)
            order.user = User.objects.get(id=user_id)
            order.save()

        # order tax, total
        order_amount, order_tax, order_total = 0, 0, 0

        # if we get some data to work with from front-end
        for key in self.request.POST.keys():
            # bookkeeping variables
            item_amount = 0

            # for each variety of item; create new order_item; then save in db
            if key.startswith("input-cart-item-"):
                item_id, item_count = (int(e) for e in self.request.POST[key].split(", "))
                print(item_id, type(item_id), item_count, type(item_count))
                item = Item.objects.get(id=item_id)

                # create an order_item
                order_item = OrderItem()
                order_item.order = order
                order_item.item = item
                order_item.price = item.price
                order_item.quantity = item_count

                # save order item in db
                order_item.save()

                # calculate the cost of this order item
                item_amount += order_item.get_cost()

            # keep adding items cost to order amount
            order_amount += item_amount

        # we have looped over all the items sent back from the frontend
        # fixed 15% tax
        order_tax = order_amount * 0.15
        order_total = order_amount + order_tax

        # make some changes to the saved order by adding more information
        order.tax = round(order_tax, 2)
        order.total = round(order_total, 2)
        print(order_amount, order_tax, order_total)
        # save order in db
        order.save()

        # create a message
        messages.info(request, f"We received your order! Please wait for confirmation on your email!")
        # Fetch the user and check if he has email
        email = order.user.email
        if email:
            subject = "Hurray! You placed the order!"
            message = f"Hi {order.user.username},\n\nPlease check your order details.\n"
            message += f"\nSeller Name: {order.seller.shop_name}\nTotal: {order.total}\n\n"
            message += "You will get a notification once the order is confirmed!\n"
            message += "\nThank you,\nNish Foody"
            send_email(email, subject, message)

        # redirect to payment page
        return redirect(reverse_lazy("profile", kwargs={'tabname': 'orders'}))


################################################################################
# menu page, <created by: Bhavik, updated by: >
# ref: https://django.cowhite.com/blog/working-with-url-get-post-parameters-in-django/
# ref: https://stackoverflow.com/questions/44859636/django-templateview-and-post
################################################################################
@method_decorator(login_required, name="dispatch")
class MenuView(TemplateView):
    model = Item
    success_url = reverse_lazy("menu")
    template_name = "webapp/menu.html"

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        context["seller"] = Seller.objects.get(id=kwargs["seller_id"])
        context["items"] = Item.objects.filter(seller=context["seller"])
        context["popular_items"] = context["items"][:4]
        return context


###########################################
#                Dalon                    #
###########################################
@method_decorator(login_required, name="dispatch")
class SellerCreate(SuccessMessageMixin, CreateView):
    model = Seller
    form_class = SellerForm
    success_url = reverse_lazy("seller", kwargs={'tabname': 'home'})
    success_message = "New Seller created successfully!"
    template_name = "webapp/new_seller_form.html"

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["button_text"] = "Create"
        # kwargs.update({'user_id': self.kwargs.get('user_id')})
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["new_seller"] = True
        return context

    def form_valid(self, form):
        # print('form_valid called')
        form.instance.user = self.request.user
        return super(SellerCreate, self).form_valid(form)

    def form_invalid(self, form):
        # print('invalid')
        # print(form)
        return CreateView.form_invalid(self, form)


@method_decorator(login_required, name="dispatch")
class SellerUpdate(SuccessMessageMixin, UpdateView):
    model = Seller
    form_class = SellerForm
    success_url = reverse_lazy("seller", kwargs={'tabname': 'home'})
    success_message = "Seller successfully updated!"
    template_name = "webapp/seller_form.html"

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["button_text"] = "Update"
        return kwargs

    def get(self, request, *args, **kwargs):
        self.object = Seller.objects.filter(user=request.user)
        if self.object:
            self.object = self.object[0]
            if not self.object.approved:
                messages.error(request, "Your shop is still under review, kindly wait for approval!")
        else:
            return redirect(reverse_lazy("new_seller"))
        return super(BaseUpdateView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object = Seller.objects.filter(user=request.user)
        if self.object:
            self.object = self.object[0]
        else:
            messages.error(
                request, "Sorry! We cannot process your request at the moment, kindly contact the Administrator!"
            )
            print("Update seller: No user seller mapping!")
            return redirect(reverse_lazy("home"))
        return super(BaseUpdateView, self).post(request, *args, **kwargs)

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super(SellerUpdate, self).form_valid(form)

    def form_invalid(self, form):
        return UpdateView.form_invalid(self, form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        sellerObj = Seller.objects.get(user=self.request.user)
        context["items"] = Item.objects.filter(seller=sellerObj)
        context["orders"] = Order.objects.filter(seller=sellerObj).order_by("-created_time")
        context["orderitems"] = {}
        for order in context["orders"]:
            context["orderitems"][order.id] = OrderItem.objects.filter(order=order)
        context["tabname"] = self.kwargs.get("tabname", "profile")
        return context


@method_decorator(login_required, name="dispatch")
class OrderConfirmView(SuccessMessageMixin, View):
    def post(self, request, *args, **kwargs):
        order_id = kwargs["order_id"]
        order = Order.objects.get(id=order_id)
        order.confirmed = True
        order.save()
        messages.info(request, f"Order: {order_id} is confirmed!")
        # Fetch the user and check if he has email
        email = order.user.email
        if email:
            subject = "Hurray! Seller confirmed your order!"
            message = f"Hi {order.user.username},\n\nYour order will be ready in sometime!\n"
            message += f"\nSeller Name: {order.seller.shop_name}\n\nThank you,\nNish Foody"
            send_email(email, subject, message)
        return redirect(reverse_lazy("seller", kwargs={'tabname': 'order'}))


@method_decorator(login_required, name="dispatch")
class OrderReadyView(SuccessMessageMixin, View):
    def post(self, request, *args, **kwargs):
        order_id = kwargs["order_id"]
        order = Order.objects.get(id=order_id)
        order.ready = True
        order.save()
        messages.info(request, f"Order: {order_id} is ready!")
        # Fetch the user and check if he has email
        email = order.user.email
        if email:
            subject = "Hurray! Your order is Ready!"
            message = f"Hi {order.user.username},\n\nPlease pick your order from the seller.\n"
            message += f"\nSeller Name: {order.seller.shop_name}\n\nThank you,\nNish Foody"
            send_email(email, subject, message)
        return redirect(reverse_lazy("seller", kwargs={'tabname': 'order'}))


@method_decorator(login_required, name="dispatch")
class ItemCreate(SuccessMessageMixin, CreateView):
    model = Item
    form_class = ItemForm
    success_url = reverse_lazy("seller", kwargs={'tabname': 'item'})
    success_message = "New Item add successfully! Please wait for admin approval!"
    template_name = "webapp/new_item_form.html"

    def form_valid(self, form):
        sellerObj = Seller.objects.get(user=self.request.user)
        form.instance.seller = sellerObj
        return super(ItemCreate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["button_text"] = "Add"
        return context


@method_decorator(login_required, name="dispatch")
class ItemUpdate(SuccessMessageMixin, UpdateView):
    model = Item
    form_class = ItemForm
    success_url = reverse_lazy("seller", kwargs={'tabname': 'item'})
    success_message = "Item updated successfully!"
    template_name = "webapp/new_item_form.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["button_text"] = "Update"
        return context


################################################################################
# Profile page, <created by: Bhavik, updated by: Dalon for user orders>
# ref: https://dev.to/earthcomfy/django-update-user-profile-33ho
################################################################################
@method_decorator(login_required, name="dispatch")
class UpdateProfile(SuccessMessageMixin, UpdateView):
    model = User
    form_class = UserProfileForm
    success_url = reverse_lazy("profile")
    success_message = "User successfully updated!"
    template_name = "webapp/user_form.html"

    # check if the get request is working properly
    def get(self, request, *args, **kwargs):
        self.object = User.objects.get(id=request.user.id)
        if not self.object:
            return redirect(reverse_lazy("home"))
        return super(BaseUpdateView, self).get(request, *args, **kwargs)

    # check if the post request is working properly
    def post(self, request, *args, **kwargs):
        self.object = User.objects.get(id=request.user.id)
        if not self.object:
            messages.error(
                request, "Sorry! We cannot process your request at the moment, kindly contact the Administrator!"
            )
            return redirect(reverse_lazy("home"))
        return super(BaseUpdateView, self).post(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["orders"] = Order.objects.filter(user=self.request.user).order_by("-created_time")
        context["orderitems"] = {}
        for order in context["orders"]:
            context["orderitems"][order.id] = OrderItem.objects.filter(order=order)
        context["tabname"] = self.kwargs.get("tabname", "profile")
        return context
