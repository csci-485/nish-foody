from django import forms
from .models import Seller, Item
from django.contrib.auth.models import User
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Row, Column, Field, Fieldset
from crispy_forms.bootstrap import Accordion, AccordionGroup


class SellerForm(forms.ModelForm):
    def __init__(self, button_text, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            "shop_name",
            Field("description", style="height: 2em"),
            "address_1",
            "address_2",
            "city",
            "province",
            "zip_code",
            "phone_number",
            "email",
            "image_url",
            "cooking_time_range",
            Submit('submit', button_text)
        )

    class Meta:
        model = Seller
        fields = (
            "shop_name",
            "description",
            "address_1",
            "address_2",
            "city",
            "province",
            "zip_code",
            "phone_number",
            "email",
            "image_url",
            "cooking_time_range",
            "user"
        )
        exclude = ('user',)

class ItemForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            "item_name",
            Field("description", style="height: 2em"),
            "availability",
            "price",
            "image_url",
            "category",
            "allergy",
            Submit('submit', 'Add')
        )

    class Meta:
        model = Item
        fields = "__all__"
        exclude = ["seller", "approved", "popularity"]



################################################################################
# worked by: Bhavik, Updated by: <>
# UserProfile Form
# ref: https://stackoverflow.com/questions/6719476/django-changing-user-profile-by-forms
################################################################################
class UserProfileForm(forms.ModelForm):
    # we must use this table and fields for this form
    class Meta:
        model = User
        fields = ("email", "first_name", "last_name")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            "email",
            "first_name",
            "last_name",
        )
        self.helper.add_input(Submit('submit', 'Update'))



class CartForm(forms.ModelForm):
    # we must use this table and fields for the form
    class Meta:
        model = Item
        fields = (
            "item_name",
            "price",
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            "item_name",
            "price"
        )
