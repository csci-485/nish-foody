# Generated by Django 4.0.2 on 2022-04-03 19:55

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0005_alter_item_options_order_ready'),
    ]

    operations = [
        migrations.RenameField(
            model_name='order',
            old_name='status',
            new_name='confirmed',
        ),
    ]
