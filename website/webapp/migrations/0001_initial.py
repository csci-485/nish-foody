# Generated by Django 4.0.2 on 2022-03-17 01:11

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Allergy',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('allergy_name', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('category_name', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('item_name', models.CharField(max_length=255)),
                ('description', models.TextField(blank=True, max_length=1000, null=True)),
                ('availability', models.BooleanField(blank=True, default=True, null=True)),
                ('price', models.FloatField()),
                ('popularity', models.IntegerField(default=0)),
                ('image_url', models.ImageField(upload_to='images/')),
                ('approved', models.BooleanField(default=False)),
                ('allergy', models.ManyToManyField(blank=True, default=None, to='webapp.Allergy')),
                ('category', models.ManyToManyField(blank=True, default=None, to='webapp.Category')),
            ],
        ),
        migrations.CreateModel(
            name='OpenHour',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('weekday', models.IntegerField(choices=[(1, 'Monday'), (2, 'Tuesday'), (3, 'Wednesday'), (4, 'Thursday'), (5, 'Friday'), (6, 'Saturday'), (7, 'Sunday')], unique=True)),
                ('from_hour', models.TimeField()),
                ('to_hour', models.TimeField()),
            ],
        ),
        migrations.CreateModel(
            name='Seller',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('shop_name', models.CharField(max_length=255)),
                ('description', models.TextField(blank=True, max_length=1000, null=True)),
                ('address_1', models.CharField(max_length=128, verbose_name='address')),
                ('address_2', models.CharField(blank=True, max_length=128, verbose_name="address cont'd")),
                ('city', models.CharField(default='Antigonish', max_length=64, verbose_name='city')),
                ('province', models.CharField(default='NS', max_length=64, verbose_name='Province')),
                ('zip_code', models.CharField(default='B2G 2C1', max_length=16, verbose_name='zip code')),
                ('image_url', models.ImageField(upload_to='images/')),
                ('rating', models.IntegerField(default=0)),
                ('cooking_time_range', models.CharField(default='30 min - 45 min', max_length=255)),
                ('approved', models.BooleanField(default=False)),
                ('open_hour', models.ManyToManyField(blank=True, default=None, to='webapp.OpenHour')),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('status', models.BooleanField(default=False)),
                ('created_time', models.DateTimeField(auto_now_add=True)),
                ('tax', models.FloatField(blank=True, default=0.0, null=True)),
                ('total', models.FloatField(blank=True, default=0.0, null=True)),
                ('items', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='webapp.item')),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('-created_time',),
            },
        ),
        migrations.AddField(
            model_name='item',
            name='seller',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='webapp.seller'),
        ),
        migrations.CreateModel(
            name='Address',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('address_1', models.CharField(max_length=128, verbose_name='address')),
                ('address_2', models.CharField(blank=True, max_length=128, verbose_name="address cont'd")),
                ('city', models.CharField(default='Antigonish', max_length=64, verbose_name='city')),
                ('province', models.CharField(default='NS', max_length=64, verbose_name='Province')),
                ('zip_code', models.CharField(default='B2G 2C1', max_length=16, verbose_name='zip code')),
                ('order', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='webapp.order')),
                ('user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
