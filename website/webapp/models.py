from django.contrib.auth.models import User
from django.db import models
from django.core.mail import send_mail
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.conf import settings

from django.core.validators import MinValueValidator


############################
# Models: Dalon            #
############################

# For business hours
WEEKDAYS = [
    (1, "Monday"),
    (2, "Tuesday"),
    (3, "Wednesday"),
    (4, "Thursday"),
    (5, "Friday"),
    (6, "Saturday"),
    (7, "Sunday"),
]


class OpenHour(models.Model):
    weekday = models.IntegerField(choices=WEEKDAYS, unique=False)
    from_hour = models.TimeField()
    to_hour = models.TimeField()

    def __str__(self):
        return f"{WEEKDAYS[self.weekday-1][1]}={self.from_hour}-{self.to_hour}"


# Seller is local seller/provider in our case. Can be Restaurant.
# This table holds all the information like seller name
# description, images, etc.
class Seller(models.Model):
    # Each seller must be linked to User
    id = models.AutoField(primary_key=True)
    shop_name = models.CharField(max_length=255)  # Seller company Name
    description = models.TextField(max_length=1000, null=True, blank=True)
    address_1 = models.CharField("address", max_length=128)
    address_2 = models.CharField("address cont'd", max_length=128, blank=True)
    city = models.CharField("city", max_length=64, default="Antigonish")
    province = models.CharField("Province", max_length=64, default="NS")
    zip_code = models.CharField("zip code", max_length=16, default="B2G 2C1")
    phone_number = models.CharField("Phone number", max_length=40, default="")
    email = models.EmailField("Email", max_length=254, default="nishfoody@gmail.com")
    image_url = models.ImageField(upload_to="images/")
    rating = models.IntegerField(default=0)
    cooking_time_range = models.CharField(max_length=255, default="30 min - 45 min")
    approved = models.BooleanField(default=False)  # Don't show unless approved by admin

    # Foreign key constraints
    user = models.OneToOneField(User, null=False, blank=False, on_delete=models.CASCADE)
    open_hour = models.ManyToManyField(OpenHour, blank=True, default=None)

    def __str__(self):
        return f"Seller: {self.shop_name}"


# Category the food item belongs to.
class Category(models.Model):
    id = models.AutoField(primary_key=True)
    category_name = models.CharField(max_length=255)

    def __str__(self):
        return f"Category: {self.category_name}"


# Allergy alert.
class Allergy(models.Model):
    id = models.AutoField(primary_key=True)
    allergy_name = models.CharField(max_length=255)

    def __str__(self):
        return f"Allergy: {self.allergy_name}"


# All items belonging to the menu
class Item(models.Model):
    id = models.AutoField(primary_key=True)
    item_name = models.CharField(max_length=255, blank=False)
    description = models.TextField(max_length=1000, null=True, blank=True)
    # proposed name changed by Bhavik: availability → available
    availability = models.BooleanField(default=True, null=True, blank=True)
    price = models.FloatField(validators=[MinValueValidator(0.0)])
    # Popularity can be used to filter popular items
    popularity = models.IntegerField(default=0)
    image_url = models.ImageField(upload_to="images/")
    approved = models.BooleanField(default=False)  # Don't show unless approved by admin

    # Foreign key constraints
    seller = models.ForeignKey(Seller, blank=False, null=False, on_delete=models.CASCADE)
    category = models.ManyToManyField(Category, blank=True, default=None)
    allergy = models.ManyToManyField(Allergy, blank=True, default=None)

    # To order things based on popularity; most popular should come first
    # -- change proposed by: Bhavik
    class Meta:
        ordering = ["-availability", "-popularity"]

    # display few things about items when using shell
    def __str__(self):
        return f"Seller: {self.seller.id}, Item: {self.item_name}"


# table for the order
class Order(models.Model):
    id = models.AutoField(primary_key=True)
    # to check if order is confirmed or not
    confirmed = models.BooleanField(default=False)
    # to check if order is ready or not
    ready = models.BooleanField(default=False)
    # data and the time of the order
    created_time = models.DateTimeField(auto_now_add=True)
    tax = models.FloatField(null=True, blank=True, default=0.0)
    total = models.FloatField(null=True, blank=True, default=0.0)
    paid = models.BooleanField(default=False)

    # Foreign key relationships
    user = models.ForeignKey(User, null=False, blank=False, on_delete=models.CASCADE)
    seller = models.ForeignKey(Seller, null=False, blank=False, on_delete=models.CASCADE, default=1)

    def __str__(self):
        return f"User: {self.user.username} Order ID: {self.id}"

    class Meta:
        ordering = ("-created_time",)


class OrderItem(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    price = models.DecimalField(max_digits=10, decimal_places=2, default=1)
    quantity = models.PositiveIntegerField(default=1)

    def __str__(self):
        return f"Orderid:{self.order.id}:Itemid:{self.item.id}"

    def get_cost(self):
        return self.price * self.quantity


# Delivery Address
class Address(models.Model):
    id = models.AutoField(primary_key=True)
    address_1 = models.CharField("address", max_length=128)
    address_2 = models.CharField("address cont'd", max_length=128, blank=True)
    city = models.CharField("city", max_length=64, default="Antigonish")
    province = models.CharField("Province", max_length=64, default="NS")
    zip_code = models.CharField("zip code", max_length=16, default="B2G 2C1")

    # Foreign key relationships
    order = models.ForeignKey(Order, null=True, blank=True, on_delete=models.CASCADE)
    user = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return f"User: {self.user.username} Address ID: {self.id}"


##################################
#           Dalon                #
##################################
# Send email when new seller is added!
@receiver(post_save, sender=Seller)
@receiver(post_save, sender=Item)
def send_new_seller_notification_email(sender, instance, created, **kwargs):
    """If a new Seller/Item is created/updated, compose and send the email"""
    try:
        if sender.__name__ == "Seller":
            name = instance.shop_name if instance.shop_name else "No name given"
        elif sender.__name__ == "Item":
            name = instance.item_name if instance.item_name else "No name given"

        message = "Hi Admin,\n\n"
        if created:
            subject = f"APPROVAL REQUEST: New {sender.__name__}: {name} create!"
            message += f"A New {sender.__name__}: {name} has been added!\nKindly approve it!\n"
            message += f"\nApproval link: {settings.DEV_SERVER_URL}/admin/webapp/{sender.__name__.lower()}/{instance.id}/change/\n"
        else:
            subject = f"UPDATE ALERT: {sender.__name__}: {name} Update!"
            message += f"A update has be done to {sender.__name__}: {name}!\nKindly review it!\n"
            message += f"\nApproval link: {settings.DEV_SERVER_URL}/admin/webapp/{sender.__name__.lower()}/{instance.id}/change/\n"

        # Add signature
        message += """\n\nBest,\nNish Foody"""

        send_mail(
            subject,
            message,
            settings.EMAIL_HOST_USER,
            settings.EMAIL_TO,
            fail_silently=True,
        )
        print(f"Email triggered for update/add in {sender.__name__} Model!")
    except Exception as e:
        print(e)
