# Register your models here.
from django.contrib import admin

from .models import Seller, Category, Allergy, Item, Order, Address, OpenHour, OrderItem

@admin.register(OpenHour)
class OpenHourAdmin(admin.ModelAdmin):
    list_display = ["weekday", "from_hour", "to_hour"]

@admin.register(Seller)
class SellerAdmin(admin.ModelAdmin):
    list_display = ["shop_name", "rating"]


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    pass


@admin.register(Allergy)
class AllergyAdmin(admin.ModelAdmin):
    pass


@admin.register(Item)
class ItemAdmin(admin.ModelAdmin):
    list_display = ["item_name", "seller", "approved"]


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    pass


@admin.register(Address)
class AddressAdmin(admin.ModelAdmin):
    pass

@admin.register(OrderItem)
class OrderItemAdmin(admin.ModelAdmin):
    pass
