from django.contrib.auth.views import LogoutView
from django.urls import path

from .views import (
    HomePage,
    CustomLoginView,
    RegisterPage,
    CartView,
    MenuView,
    SearchPage,
    SellerPage,
    SellerCreate,
    SellerUpdate,
    UpdateProfile,
    OrderConfirmView,
    OrderReadyView,
    ItemCreate,
    ItemUpdate,
    PaymentInfoView,
    CartCheckoutView
)

urlpatterns = [
    path("", HomePage.as_view(), name="home"),
    # path("home_seller/", SellerHome.as_view(), name="home_seller"),
    path("payment/", PaymentInfoView.as_view(), name="payment"),
    # path("seller/", SellerPage.as_view(), name="seller"),
    path("login/", CustomLoginView.as_view(), name="login"),
    path("logout/", LogoutView.as_view(next_page="home"), name="logout"),
    path("register/", RegisterPage.as_view(), name="register"),
    path("cart/", CartView.as_view(), name="cart"),
    path("cart_checkout/", CartCheckoutView.as_view(), name="cartcheckout"),
    path("menu/<int:seller_id>", MenuView.as_view(), name="menu"),
    path("seller/new", SellerCreate.as_view(), name="new_seller"),
    path("seller/<str:tabname>", SellerUpdate.as_view(), name="seller"),
    path('search/', SearchPage.as_view(), name='search'),
    path("profile/<str:tabname>", UpdateProfile.as_view(), name="profile"),
    path("order_confirm/<int:order_id>", OrderConfirmView.as_view(), name="orderconfirm"),
    path("order_ready/<int:order_id>", OrderReadyView.as_view(), name="orderready"),
    path("item/new", ItemCreate.as_view(), name="new_item"),
    path("item/update/<int:pk>", ItemUpdate.as_view(), name="update_item")
]

