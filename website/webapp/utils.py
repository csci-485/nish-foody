from django.core.mail import send_mail
from django.conf import settings


def send_email(email_to, subject, message):
    try:
        send_mail(
            subject,
            message,
            settings.EMAIL_HOST_USER,
            (email_to,),
            fail_silently=True,
        )
        print(f"Email sent to {email_to}")
    except Exception as e:
        print(e)
