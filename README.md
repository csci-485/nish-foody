# Nish Foody

---

## Description

Antigonish needs a software to help:

1. Houses offer their delicious food dishes
2. Customers buy and get their orders

---

## TOP Features

1. The newly added seller and item will not be shown until it is approved by the Administrator. An administrator will
   receive and email reminder to approve the request.
2. The sellers have control make their items unavailable.
3. The popular items by the seller has a dedicated section where 4 top items based on popularity will appear.
4. The users as buyers can update their details like first name, last name, and email from the profile.
5. The general sellers details can be updated from their profile.
6. The users can use chatbot to gain some answers/queries they have.
7. The users can search using different criteria.

___

## Method to run the website

### Method-1 Access directly using our website URI:

1. Production server: [NishFoody](https://nish-foody.herokuapp.com)
2. Development server: [NishFoody-dev](https://nish-foody-dev.herokuapp.com)

### Method-2 Run locally on your system.

1. Create a directory and navigate to it
    1. linux: `mkdir 485project && cd 485project`
    2. windows/mac: use GUI
2. Create a virtual environment: `python3 -m venv 485venv/`
3. Activate the virtual environment: `source 485venv/bin/activate`
4. Link of the repository: [bitbucket repository](https://bitbucket.org/csci-485/nish-foody/src/main/)
5. Clone command: `git clone https://bitbucket.org/csci-485/nish-foody.git`
6. Navigate to the project directory: `cd nish-foody`
7. Install the required packages: `pip3 install -r requirements.txt`
8. Navigate to the website directory `cd website`
9. Run the server locally: `python manage.py runserver`

## Useful django commands

1. Run the server: `python manage.py runserver`. This command will run the development server locally.
2. Initial populate the db: `python manage.py loaddata webapp/fixtures/webapp.json`.
3. Open the shell: `python manage.py shell`.

## Steps to follow while working locally

1. `git status` Make sure you are on the correct branch
2. If not change the branch using: `git checkout branch_name`
3. Add your files to staging: `git add file_name`
4. Commit all your previous changes: `git commit -m "Your Commit Message"`
5. Sync your repo with dev branch: `git pull origin release/dev`
6. Work on your tasks.
7. Before ending for the day commit your work. Same drill as steps 1-4.
8. Push your work using: `git push`. Make sure you are on the correct branch.

## Project folder structure

```
├── config
│   ├── __init__.py
│   ├── asgi.py
│   ├── settings.py
│   ├── urls.py
│   └── wsgi.py
├── manage.py
├── media
│   └── images
├── static
│   ├── admin
│   └── webapp
└── webapp
    ├── __init__.py
    ├── admin.py
    ├── apps.py
    ├── fixtures
    │   ├── fixtures_template.json
    │   └── webapp.json
    ├── forms.py
    ├── migrations
    │   ├── __init__.py
    ├── models.py
    ├── static
    │   └── webapp
    ├── templates
    │   └── webapp
    │       ├── footer.html
    │       ├── home.html
    │       ├── login.html
    │       ├── main.html
    │       ├── menu_page.html
    │       ├── navbar.html
    │       └── register.html
    ├── tests
    │   ├── __init__.py
    │   └── test_models.py
    ├── urls.py
    └── views.py
```

## Known Issues

1. Low emphasis on type of authentication. We use basic authentication, since, changing or adding any new authentication
   is just a matter of including a module. For example adding oauth or 2 factor authentication can be added without
   large overhead or implementation time.
2. Low emphasis given to payment pipeline since it can be added without large overhead given all the third party payment
   integrations are made fairly easy.
   Package `Braintree` [Integration documentation](https://developer.paypal.com/braintree/docs/guides/transactions/python)
   or `dj-stripe` [Integration documentation](https://developer.paypal.com/braintree/)
   . [Click here for Stripe tutorial here.](https://testdriven.io/blog/django-stripe-tutorial/)
3. Uploading an image on production server, running on Heroku, doesn't store image for long time. Since the dyno's
   running the django app are reset every 30 minutes. Resolution is to use S3 buckets. Currently out of
   scope. [Link to documentation](https://devcenter.heroku.com/articles/s3)

## Docker run command

1. `docker build -t nishfoody .`
2. `docker rm nishfoody`
3. `docker run -dp 3000:3000 --name nishfoody nishfoody`
4. `docker start nishfoody`

# How to run using the docker

Please follow the following steps to run the docker image from docker hub.

1. First pull the docker image form Docker Hub: `docker pull dalonlobo/nishfoody:latest`
2. Now the docker image can be easily run using this
   command: `docker run -dp 3000:3000 --name nishfoody dalonlobo/nishfoody:latest`
3. Access our project from browser using <http://localhost:3000/>
4. If you want to stop the docker, run: `docker stop nishfoody`
5. To start the docker run: `docker start nishfoody`

# Steps for database integration

We are running postgresql as our database. So the following instructions can be followed to integrate the db with our project.

1. First start a postgresql database. The instructions to download and run on linux system are [here](https://linuxhint.com/start-postgresql-linux/)
2. Create a postgresql database user. EX: `sudo -u postgres createuser <username>`. Detailed steps are [here](https://medium.com/coding-blocks/creating-user-database-and-adding-access-on-postgresql-8bfcd2f4a91e)
3. Create a database for our project. EX: `sudo -u postgres createdb <dbname>`. Detailed steps are [here](https://medium.com/coding-blocks/creating-user-database-and-adding-access-on-postgresql-8bfcd2f4a91e)
4. Now all we need to do is add these instructions to the settings.py file as shown below:
    The settings.py is located in the project nish-foody/website/config directory.

    On line number 100, change the DATABASE settings and update according to the db, user and password with correct PORT number and the location where the db is running.
    EX:
        DATABASES = {
        "default": {
            "ENGINE": "django.db.backends.postgresql_psycopg2",
            "NAME": "d82piu6dshcmse",
            "USER": "kbzazbkxbadukp",
            "PASSWORD": "xxx",
            "HOST": "ec2-54-85-113-73.compute-1.amazonaws.com",
            "PORT": "5432",
        }
    }
