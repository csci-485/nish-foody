FROM python:3.9.5
RUN mkdir -p /home/ubuntu/nishfoody/
WORKDIR /home/ubuntu/nishfoody/
ADD . .
RUN pip3 install -r requirements.txt
WORKDIR /home/ubuntu/nishfoody/website/
EXPOSE 8000
CMD ["gunicorn", "config.wsgi", "--bind=0.0.0.0:3000", "--workers=3"]
